<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //

    protected $fillable=['id','title','description','url','course_id'];
    public $timestamps=false;

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
