<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscribtion extends Model
{
    //
    protected $fillable = ['started_at', 'end_at', 'user_id', 'plan_id'];

    public $timestamps = false;

    protected $dates = ['started_at', 'end_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }
}
