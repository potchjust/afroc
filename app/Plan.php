<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    //
    public $timestamps = false;
    public $fillable = ['name', 'duration'];

    public function plans()
    {
        return $this->hasMany(Subscribtion::class);
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }

}
