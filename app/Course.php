<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //
    protected $fillable = ['id', 'title', 'description', 'featured_image', 'rank', 'teacher_id'];
    public $timestamps = false;

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function plans()
    {
        return $this->belongsToMany(Plan::class);
    }

}
