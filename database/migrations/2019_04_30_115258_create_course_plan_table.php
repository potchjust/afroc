<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursePlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->integer('plan_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('plan_id')->references('id')->on('plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_plan', function (Blueprint $blueprint) {
            $blueprint->dropForeign('course_plan_course_id_foreign');
            $blueprint->dropForeign('course_plan_plan_id_foreign');
            $blueprint->dropColumn('course_id');
            $blueprint->dropColumn('plan_id');

        });
        Schema::dropIfExists('course_plan');
    }
}
