<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribtionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribtions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('started_at');
            $table->date('end_at');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on('plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscribtions', function (Blueprint $blueprint) {
            $blueprint->dropForeign('subscribtions_user_id_foreign');
            $blueprint->dropForeign('subscribtions_plan_id_foreign');
            $blueprint->dropColumn('user_id');
            $blueprint->dropColumn('plan_id');
        });
        Schema::dropIfExists('subscribtions');
    }
}
